<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('login', function() {
    return redirect('admin/home');
});

// Pages
Route::get('/admin/home', function () {
    return view('admin/home');
});
Route::get('/admin/head-image', function () {
    return view('admin/head-image');
});
Route::get('/admin/users', function () {
    return view('admin/users');
});
Route::get('/admin/content', function () {
    return view('admin/content');
});
Route::get('/admin/benefit', function () {
    return view('admin/benefit');
});
Route::get('/admin/footer', function () {
    return view('admin/footer');
});
