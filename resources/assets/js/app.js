
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('head-image', require('./components/HeadImage.vue'));
Vue.component('content-form', require('./components/ContentForm.vue'));
Vue.component('benefit-form', require('./components/BenefitForm.vue'));
Vue.component('user-list', require('./components/UserList.vue'));
Vue.component('footer-form', require('./components/FooterForm.vue'));
Vue.component('dashboard-component', require('./components/DashboardComponent.vue'));

const app = new Vue({
    el: '#app'
});
