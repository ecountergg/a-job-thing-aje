const ALLOWED = /(\.jpeg|\.jpg|\.png)$/i;

const validationFile = (path, allowed = null) => {
    if (!allowed) allowed = ALLOWED;

    if (!allowed.exec(path)) {
        return false;
    }
    return true;
}

export default validationFile;
